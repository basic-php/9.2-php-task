<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Dasar</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h1>Informasi Mahasiswa UAD</h1>
        <?php
        $nim = "2300018332";
        $nama = "Saif Brilliantoro";
        $fakultas = "Teknologi Industri";
        $jurusan = "Informatika";
        $umur = 19;
        $nilai = 4;
        $status = TRUE;
        ?>
        <table>
            <tr>
                <th>NIM</th>
                <td><?php echo $nim; ?></td>
            </tr>
            <tr>
                <th>Nama</th>
                <td><?php echo $nama; ?></td>
            </tr>
            <tr>
                <th>Fakultas</th>
                <td><?php echo $fakultas; ?></td>
            </tr>
            <tr>
                <th>Jurusan</th>
                <td><?php echo $jurusan; ?></td>
            </tr>
            <tr>
                <th>Umur</th>
                <td><?php echo $umur; ?></td>
            </tr>
            <tr>
                <th>IPK</th>
                <td><?php printf("%.3f", $nilai); ?></td>
            </tr>
            <tr>
                <th>Status</th>
                <td><?php echo $status ? "Aktif" : "Tidak Aktif"; ?></td>
            </tr>
        </table>
    </div>
</body>
</html>
